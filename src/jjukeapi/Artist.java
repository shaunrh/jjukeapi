package jjukeapi;

import java.awt.Image;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Artist {
	//protected String name = null;
	//protected String key = null;
	protected String name;
	protected String artists_url = null;
	protected ArrayList<Image> imgs = new ArrayList<Image>();
	protected ArrayList<String> lastfmtags = new ArrayList<String>();
	
	public Artist(){
	}
	void getInfo(String key, String name, boolean autocorrect) throws IOException, JSONException{
		//http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&artist=Cher&api_key=074e34ca5e265fd19982de245af03154&format=json
		String urlargs = new String();
		urlargs = "http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&artist=" + name + "&api_key=" + key + "&format=json";
		if(autocorrect) urlargs = urlargs + "&autocorrect=1";
		URL apiurl = new URL(urlargs);
		URLConnection apiconn = apiurl.openConnection();
		BufferedReader in = new BufferedReader(new InputStreamReader(apiconn.getInputStream()));
		String result = new String();
		while (in.ready()) {
			result += in.readLine();
		}
		in.close();

		JSONObject job = new JSONObject(result);
		
		//get the accepted name(because it could have potentially been autocorrected)
		this.name = ((JSONObject)job.get("artist"))
									 .getString("name");
		
		
		//get their lastfm profile url
		artists_url = ((JSONObject) job.get("artist"))
										.getString("url");
		
		//get the artists "tags" (not id3) according to lastfm
		for(int i = 0; i < 5; i++){
			JSONObject tags = ((JSONObject) job.get("artist")).getJSONObject("tags");
			JSONArray tag = tags.getJSONArray("tag");
			lastfmtags.add(((JSONObject) tag.get(i)).getString("name"));
			System.out.println(((JSONObject) tag.get(i)).get("name"));
		}
		
		//get their small, medium, and large Images and store them in an ArrayList
		for(int i = 0; i < 5; i++){
			JSONArray imglinks = ((JSONObject) job.get("artist")).getJSONArray("image");
			JSONObject img = imglinks.getJSONObject(i);
			String picurl = img.getString("#text");
			Image image = null;
	        try {
	            URL imgurl = new URL(picurl.toString());
	            image = ImageIO.read(imgurl);
	        } catch (IOException e) {
	        	e.printStackTrace();
	        }
			imgs.add(image);
			
			//just to see if the images it pulled worked
	        JFrame frame = new JFrame();
	        frame.setSize(300, 300);
	        JLabel label = new JLabel(new ImageIcon(image));
	        frame.add(label);
	        frame.setVisible(true);
		}

	}
}
