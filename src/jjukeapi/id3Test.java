package jjukeapi;

import java.io.File;
import java.io.IOException;
import javax.swing.JFileChooser;
import com.mpatric.mp3agic.ID3v1;
import com.mpatric.mp3agic.ID3v2;
import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.UnsupportedTagException;

public class id3Test {

	public static void main(String[] args) throws UnsupportedTagException, InvalidDataException, IOException {
		JFileChooser jfc = new JFileChooser();
		jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		if(jfc.showOpenDialog(null) != JFileChooser.APPROVE_OPTION){
			log("No file chosen. Exiting.");
			System.exit(0);
		}
		File dirpath = jfc.getSelectedFile();
		log("In dir: " + dirpath);
		File[] mp3s = dirpath.listFiles();

		for(int i = 0; i < mp3s.length; i++){

			if(mp3s[i].getName().contains(".mp3"))
				printInfo(mp3s[i]);
		}
	}
	private static void printInfo(File song) throws UnsupportedTagException, InvalidDataException, IOException{

		//check tags
		log("Found song: " + song.getName());
		Mp3File mp3file = new Mp3File(song.getAbsolutePath());
		log("Length of this mp3 is: " + mp3file.getLengthInSeconds() + " seconds");
		log("Bitrate: " + mp3file.getLengthInSeconds() + " kbps " + (mp3file.isVbr() ? "(VBR)" : "(CBR)"));
		log("Sample rate: " + mp3file.getSampleRate() + " Hz");
		log("Has ID3v1 tag?: " + (mp3file.hasId3v1Tag() ? "YES" : "NO"));
		log("Has ID3v2 tag?: " + (mp3file.hasId3v2Tag() ? "YES" : "NO"));
		log("Has custom tag?: " + (mp3file.hasCustomTag() ? "YES" : "NO"));
		log("-------------------------");
		//get tags of type Id3v1

		if (mp3file.hasId3v1Tag()) {
			log("Printing id3v1 tags for: " + song.getName());
			ID3v1 id3v1Tag = mp3file.getId3v1Tag();
			log("Track: " + id3v1Tag.getTrack());
			log("Artist: " + id3v1Tag.getArtist());
			log("Title: " + id3v1Tag.getTitle());
			log("Album: " + id3v1Tag.getAlbum());
			log("Year: " + id3v1Tag.getYear());
			log("Genre: " + id3v1Tag.getGenre() + " (" + id3v1Tag.getGenreDescription() + ")");
			log("Comment: " + id3v1Tag.getComment());
			log("-------------------------");
		}

		//get tags of type Id3v2
		
		if (mp3file.hasId3v2Tag()) {
			log("Printing id3v2 tags for: " + song.getName());
			ID3v2 id3v2Tag = mp3file.getId3v2Tag();
			log("Track: " + id3v2Tag.getTrack());
			log("Artist: " + id3v2Tag.getArtist());
			log("Title: " + id3v2Tag.getTitle());
			log("Album: " + id3v2Tag.getAlbum());
			log("Year: " + id3v2Tag.getYear());
			log("Genre: " + id3v2Tag.getGenre() + " (" + id3v2Tag.getGenreDescription() + ")");
			log("Comment: " + id3v2Tag.getComment());
			log("Composer: " + id3v2Tag.getComposer());
			log("Publisher: " + id3v2Tag.getPublisher());
			log("Original artist: " + id3v2Tag.getOriginalArtist());
			log("Album artist: " + id3v2Tag.getAlbumArtist());
			log("Copyright: " + id3v2Tag.getCopyright());
			log("URL: " + id3v2Tag.getUrl());
			log("Encoder: " + id3v2Tag.getEncoder());
			log("-------------------------");
			byte[] albumImageData = id3v2Tag.getAlbumImage();
			if (albumImageData != null) {
				log("Have album image data, length: " + albumImageData.length + " bytes");
				log("Album image mime type: " + id3v2Tag.getAlbumImageMimeType());
			}
		}
	}
	private static void log(String in){
		System.out.println(in);
	}
}
