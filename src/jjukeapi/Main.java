package jjukeapi;

import java.io.IOException;
import java.util.Scanner;

import org.json.JSONException;

public class Main {
	public static void main(String[] args) throws IOException, JSONException{
		String key = "074e34ca5e265fd19982de245af03154";
		Scanner in = new Scanner(System.in);
		System.out.print("Get info on (artist): ");
		String name = in.nextLine();
		Artist art = new Artist();
		name = name.replaceAll(" ", "%20");
		art.getInfo(key,name,true);
		
		System.out.println("official lastfm api name:" + art.name);
		System.out.println("artists profile url: " + art.artists_url);
		
		
	}
}
